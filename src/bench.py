#!/usr/bin/env python3

import os
import time
import subprocess
import random

NITEMS = [1]
        
with open("name-basics.tsv", "r") as n:
    name = []
    for line in n:
        name.append(line.split("\t")[1])


for items in NITEMS:
    start = time.time()

    for i in range(0, items):
        actor1 = random.randrange(1, len(name))
        actor2 = random.randrange(1, len(name))

    print(name[actor1])
    print(name[actor2])

    command = "./movie_script.py\n" + name[actor1] + "\n" + name[actor2] + "\n"

    p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)

    while True:
        result = p.poll()
        if result is not None:
            break
        time.sleep(0.5)

    end = time.time()
    totalTime = start - start
    print('NITEMS: {}, Time: {}'.format(items, totalTime))