#!/usr/bin/env python3
from collections import defaultdict
import re
import json
import ast


filename = "name-basics.tsv"
titlefile = "data-titles.tsv"
degrees_of_separation = 30

with open(filename, "r") as f:
        initial_data = [line.split("\t") for line in f]
        data = tuple((item[1], item[2].strip("\"").strip("\n").strip("\"").split(",")) for item in initial_data)
        data = dict(data)

with open(titlefile, "r") as t:
        title_data = [line.split("\t") for line in t]
        titles = tuple((item[0], item[2].strip("\"").strip("\n").strip("\"").split(",")) for item in title_data)
        titles = dict(titles)


def invert_map(mapping):
    new_mapping = defaultdict(list)
    for key in mapping.keys():
        for value in mapping[key]:
            new_mapping[value].append(key)
    return new_mapping


def common_movies_and_actors(actor1, actor2, actorMap, movieMap):
    '''
    Find the common movies between two actors
    '''
    combined_graph = {**actorMap, **movieMap}
    try:
        actor1_movies = actorMap[actor1]
        print("{} movies: {}".format(actor1, actor1_movies))
    except:
        pass
    try:
        actor2_movies = actorMap[actor2]
        print("{} movies: {}".format(actor2, actor2_movies))
    except:
        pass
    intersection = [movie for movie in actor1_movies if movie in actor2_movies]
    if len(intersection) > 0:
        return intersection
    else:
        errstring = print(
        "This dataset does not contain movies in common between {} and {}"
            .format(actor1, actor2)
        )
        common_actors(combined_graph, actor1, actor2, degrees_of_separation)
        return errstring


def common_actors(graph, starting_point, end_point, maximum_depth):
    '''
    graph traversal where the graph is a combination of Two
    other graphs. In our case, these two graphs will be our movie
    graph and our actor graph.
    '''
    queue = []
    depth = 0
    visited = set()
    # print(graph[starting_point])
    queue.append([starting_point])

    while len(queue) > 0: # and depth <= maximum_depth:
        path = queue.pop(0)
        node = path[-1]
        if node == end_point:
            # print(path)
            return path
        elif node not in visited:
            for adjacent in graph[node]:
                new_path = list(path)
                new_path.append(adjacent)
                queue.append(new_path)
            visited.add(node)
        depth += 1


    # if found_path is False:
    #     return ("""
    #     No close relation found between the two actors!
    #     Checked for a relationship within {} degrees
    #     between: {} and {}
    #     """.format(degrees_of_separation, starting_point, end_point)
    #     )

def jsonify(path, movieMap, actorMap, visited):
    d3 = {}
    for item in path:
        if item in movieMap.keys():
            if item not in visited:
                if item == path[0]:
                    visited.add(item)
                    t = list()
                    count = 0
                    for pitem in movieMap[item]:
                        if pitem not in visited:
                            count = count +1
                            r = (jsonify(path, movieMap, actorMap, visited))
                            if len(r) > 0:
                                t.append(r)
                            else :
                                r = {
                                    "name": titles[pitem],
                                    "parent": path[path.index(item)-1] if item != path[0] else "null"
                                }
                                t.append(r)
                            if count > 5:
                                break
                    if len(t) > 0:
                        d3.update( {
                            "name": item,
                            "parent": path[path.index(item)-1] if item != path[0] else "null",
                            "children": t
                            } )
                    else:
                        d3.update( {
                            "name": item,
                            "parent": path[path.index(item)-1] if item != path[0] else "null"
                            } )
                else:
                    visited.add(item)
                    t = list()
                    count = 0
                    for pitem in movieMap[item]:
                        if pitem not in visited:
                            count = count +1
                            r = (jsonify(path, movieMap, actorMap, visited))
                            if len(r) > 0:
                                t.append(r)
                            else:
                                r = {
                                    "name": titles[pitem],
                                    "parent": path[path.index(item)-1] if item != path[0] else "null"
                                }
                                t.append(r)
                            if count > 5:
                                break
                    if len(t) > 0: 
                        d3.update( {
                            "name": item,
                            "parent": path[path.index(item)-1] if item != path[0] else "null",
                            "children": t 
                            } )
                    else:
                         d3.update( {
                            "name": item,
                            "parent": path[path.index(item)-1] if item != path[0] else "null"
                            } )

            else:
                continue
            # d3 += " {"
            # d3 += '\n   "name":   {},\n   "children":   {},\n   "parent":    {}\n'.format(item, movieMap[item], path[path.index(item)-1])
            # d3 += " },\n"
        elif item in actorMap.keys():
            if item not in visited:
                visited.add(item)
                t = list()
                count = 0
                for pitem in actorMap[item]:
                    if pitem not in visited:
                        count = count + 1
                        r = (jsonify(path, movieMap, actorMap, visited))
                        if len(r) > 0:
                            t.append(r)
                        else:
                            r = {
                                "name": pitem,
                                "parent": path[path.index(item)-1]
                            }
                            t.append(r)
                        if count > 5:
                            break
                if len(t) > 0:
                    d3.update( {
                        "name": titles[item],
                        "parent": path[path.index(item)-1],
                        "children": t
                    } )
                else:
                    d3.update( {
                        "name": titles[item],
                        "parent": path[path.index(item)-1]
                    } )
            else:
                continue
            # d3 += " {"
            # d3 += '\n   "name":   {},\n   "children":   {},\n   "parent":    {}\n'.format(item, actorMap[item], path[path.index(item)-1])
            # d3 += " },\n"
        else:
            continue
    # d3 += "\n]"
    return d3


mydict = {
    'a':    [1,2,3],
    'b':    [4,3,6]
}

actor1 = input();
actor2 = input();

full_graph = {**data, **invert_map(data)}
emptyL = []
my_list = common_actors(full_graph, actor1, actor2, degrees_of_separation)

empty = set()
d3 = jsonify(my_list, data, invert_map(data), empty)
print("[" + json.dumps(d3, indent=4) + "]")
