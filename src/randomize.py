#!/usr/bin/env python3

import os
import time
import subprocess
import random


with open("name-basics.tsv", "r") as n:
    name = []
    for line in n:
        name.append(line.split("\t")[1])


start = time.time()

actor1 = random.randrange(1, len(name))
actor2 = random.randrange(1, len(name))

print(name[actor1])
print(name[actor2])