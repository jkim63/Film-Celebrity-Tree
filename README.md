Film-Celebrity-Tree

NETID:
jkim63
hfrancoi
sriordan

How to run:
1. Download all files in gitlab from src and www folders. Files must stay
   in their respective folders
   a) may have to edit trees.js on line 150 to make it fit your local/student machine
2. Run "python-server.py" and open a web browser to enter a corresponding web-sever
3. Enter the names of 2 actors
4. Press submit
5. Wait until the tree shows up.

**bench.py was not actually used. It was a program that we wanted to use to benchmark,
  but the program would not wait until each process was over, instead printed out
  time as soon as it outputted the names of the 2 actors. randomize.py and movie_script.py
  were used instead to test for time.

Member contribution:
- jkim63: algorithm, benchmarking, video, slides, cleanup
- hfrancoi: data structure, web-server refining, slides
- sriordan: algorithm, web-server, slides